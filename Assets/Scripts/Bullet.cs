using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float speed = 20f;
    public Rigidbody2D rb;

    private Vector2 screenBounds;
    private float objectWidth;
    private GameObject player;
    private PlayerData PD;
    

    // Start is called before the first frame update
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
        player = GameObject.Find("Player");
        PD = player.GetComponent<PlayerData>();
    
        rb.velocity = transform.right * speed;
    }

    void Update()
    {
        Vector3 viewPos = transform.position;
        
        if (viewPos.x != Mathf.Clamp(viewPos.x, screenBounds.x * -1 + objectWidth, screenBounds.x - objectWidth))
        {
            Destroy(gameObject);
        }    
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.tag.Equals("Player"))
        {
            Debug.Log(collision.name);
            Destroy(gameObject);
            if (collision.gameObject.tag.Equals("Enemy"))
            {
                Destroy(collision.gameObject);
                PD.kills++;
            }
        }
    }
}
