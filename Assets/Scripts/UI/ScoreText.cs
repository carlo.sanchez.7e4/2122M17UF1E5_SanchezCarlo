using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    public enum textType{
        Kills,
        Height
    }
    public textType selector;
    private Text textOutput;
    private int kills;
    private float maxHeight;

    // Start is called before the first frame update
    void Start()
    {
        textOutput = GetComponent<Text>();
        kills = PlayerPrefs.GetInt("Kills");
        maxHeight = PlayerPrefs.GetFloat("MaxHeight");
    }

    // Update is called once per frame
    void Update()
    {
        switch (selector)
        {
            case textType.Kills:
                textOutput.text = "Kills:\t" + kills;
                break;
            case textType.Height:
                textOutput.text = "Max Height:\t" + maxHeight;
                break;
        }
    }
}
