using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerData : MonoBehaviour
{

    [SerializeField]
    public float Speed;
    public bool facingLeft = false;
    public float jumpStrength;
    public bool onGround = true;
    public int HP;
    public int kills;
    public GameObject gun;


    private GameManager gameManager;
    private Weapon weapon;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        weapon = gameObject.GetComponent<Weapon>();
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Ground"))
        {
            onGround = true;
        } else if (collision.gameObject.tag.Equals("Platform")) {
            Vector2 direction = collision.GetContact(0).normal;
            if (direction.y == 1)
            {
                onGround = true;
            }
        }

    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Ground") || collision.gameObject.tag.Equals("Platform"))
        {
            onGround = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            gameManager.LostHP();
            Destroy(collision.gameObject);
        } else if (collision.gameObject.tag.Equals("HP"))
        {
            if (HP < 3)
            {
                gameManager.GainHP();
            }
            Destroy(collision.gameObject);
        } else if (collision.gameObject.tag.Equals("Pistol"))
        {
            Destroy(collision.gameObject);
            gun.SetActive(true);
            weapon.hasGun = true;
        }
    }
}
