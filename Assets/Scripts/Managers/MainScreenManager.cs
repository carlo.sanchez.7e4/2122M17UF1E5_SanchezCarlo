using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScreenManager : MonoBehaviour
{
    private static MainScreenManager _instance;

    public static MainScreenManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Main Menu Manager is NULL!");
            }

            return _instance;
        }


    }

    private void Awake()
    {

        if (_instance != null && _instance != this)
        {

            Destroy(this.gameObject);
        }

        _instance = this;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Return))
        {
            SceneManager.LoadScene("GameScreen");
        } else if (Input.GetKey(KeyCode.R) && Input.GetKey(KeyCode.LeftControl))
        {
            Debug.Log("ScoreRestored");
            PlayerPrefs.DeleteAll();
        } else if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
